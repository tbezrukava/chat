import { faHeart, faPen, faTrash, faPaperPlane } from "@fortawesome/free-solid-svg-icons";

const iconName = {
    like: faHeart,
    edit: faPen,
    delete: faTrash,
    send: faPaperPlane
}

export { iconName }