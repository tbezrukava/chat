import styles from './styles.module.scss';

const ErrorMessage = () => (
	<div className={styles.error}>
		<p>Oops... Something went wrong</p>
	</div>
)

export default ErrorMessage;