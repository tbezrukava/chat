import { FC } from 'react';
import Icon from '../icon/icon-button';
import { iconName } from '../../constants/icons';
import { HandleOnclickFunc } from '../../helpers/interfaces';
import styles from './styles.module.scss';

interface OwnMessageProps {
    id: string,
    text: string,
    createTime: string,
    isEdited: boolean,
    onDelete: HandleOnclickFunc,
    onEdit: HandleOnclickFunc
}

const OwnMessage: FC<OwnMessageProps> = ({ id, text, isEdited, createTime,
                                             onDelete, onEdit  }) => {
    const handleDelete = () => onDelete(id);
    const handleEdit = () => onEdit(id);
    
    return (
        <div className={`own-message ${styles.item}`}>
            <div>
                <div className={styles.panel}>
                    <button
                        className="message-edit"
                        type="button"
                        onClick={handleEdit}
                    >
                        <Icon iconName={iconName.edit} />
                    </button>
                    <button
                        className="message-delete"
                        type="button"
                        onClick={handleDelete}
                    >
                        <Icon iconName={iconName.delete} />
                    </button>
                </div>
                <div className={`message-text ${styles.text}`}>{text}</div>
                <div className={styles.info}>
                    {isEdited && <span>edited</span>}
                    <span className={`message-time`}>{createTime}</span>
                </div>
            </div>
        </div>
    );
}

export default OwnMessage;