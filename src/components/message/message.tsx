import { FC, MouseEvent } from 'react';
import Icon from '../icon/icon-button';
import { iconName } from '../../constants/icons';
import styles from './styles.module.scss';

interface MessageProps {
    user: string,
    avatar: string,
    text: string,
    createTime: string,
    isEdited: boolean
}

const Message: FC<MessageProps> = ({ user, avatar, text, createTime, isEdited }) => {
    const handleLike = (e: MouseEvent<HTMLButtonElement>): void => {
        if (e.currentTarget.classList.contains('message-like')) {
            e.currentTarget.classList.remove('message-like');
            e.currentTarget.classList.add('message-liked');
        } else {
            e.currentTarget.classList.remove('message-liked');
            e.currentTarget.classList.add('message-like');
        }
    }
    
    return (
        <div className={`message ${styles.item}`}>
            <div>
                <img
                    className={`message-user-avatar`}
                    src={avatar}
                    alt="avatar"
                />
                <button
                    className="message-like"
                    onClick={handleLike}
                >
                    <Icon iconName={iconName.like} />
                </button>
                <div className={`message-user-name ${styles.name}`}>{user}</div>
                <div className={`message-text ${styles.text}`}>{text}</div>
                <div className={styles.info}>
                    {isEdited && <span>edited</span>}
                    <span className={`message-time`}>{createTime}</span>
                </div>
            </div>
        </div>
    )
};

export default Message;