import { FC } from 'react';
import { HeaderData as HeaderProps } from '../../helpers/interfaces';
import styles from './styles.module.scss';

const Header: FC<HeaderProps> = ({ title, usersNum, messagesNum, lastMessageTime }) => {
    const userNoun = usersNum === 1 ? 'participant' : 'participants';
    const userContent = `${usersNum} ${userNoun}`;
    const messageNoun = messagesNum === 1 ? 'message' : 'messages';
    const messageContent = `${messagesNum} ${messageNoun}`;
    const lastMessageTimeContent = `last message ${lastMessageTime}`;
    
    return (
        <header className={`header ${styles.header}`}>
            <div className={styles.info}>
                <div className={`header-title ${styles.title}`}>{title}</div>
                <div className={`header-users-count`}>{userContent}</div>
                <div className={`header-messages-count`}>{messageContent}</div>
            </div>
            <div className={`header-last-message-date`}>{lastMessageTimeContent}</div>
        </header>
    );
};

export default Header;