import styles from './styles.module.scss'

const Preloader = () => (
    <div className={`preloader ${styles.wrapper}`}>
        <div className={styles.circle} />
        <div className={styles.circle} />
        <div className={styles.circle} />
        <div className={styles.shadow} />
        <div className={styles.shadow} />
        <div className={styles.shadow} />
        <span>Loading</span>
    </div>
);

export default Preloader;